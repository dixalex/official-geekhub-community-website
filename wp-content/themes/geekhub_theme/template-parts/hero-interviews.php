<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>
<div class="interviews">
    <h2 class="site-title">Співбесіди</h2>
    <p class="site-sub-title page">Наступний етап відбору це співбесіда</p>

    <a class="registration-button" href="/season-8/#115">Ознайомитись з правилами</a>

    <a class="about-button" href="https://www.facebook.com/geekhub.ck">Списки запрошених та розклад тут</a>
</div>
