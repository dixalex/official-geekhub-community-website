<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>
<div class="auto-testing">
    <h2 class="site-title">Реєстрацію завершено</h2>

    <a class="registration-button" href="/faq/#114">Деталі процесу тестування</a>

    <a class="about-button" href="https://www.facebook.com/geekhub.ck">Остання інформація</a>
</div>
