<div class="season-ended">
    <h2 class="site-title"><?php echo get_season_name(); ?> сезон у розпалі</h2>
    <p class="site-sub-title page">Реєстрація на <?php echo get_next_season_number_ordinal(); ?> сезон відкриється 15
        серпня <?php echo get_next_season_year(); ?></p>
</div>
