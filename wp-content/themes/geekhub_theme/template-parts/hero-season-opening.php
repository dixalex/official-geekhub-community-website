<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>
<div class="season-opening">
    <?php
    $reg_button_link = get_permalink(get_theme_mod( 'hero_register_url' ));
    $reg_button_name = get_theme_mod( 'button_registration_text' );

    $about_button_link = get_permalink(get_theme_mod( 'hero_about_season_url' ));
    $about_button_name = get_theme_mod( 'button_about_text' );
    ?>

    <h2 class="site-title">Відкриття нового сезону</h2>
    <p class="site-sub-title page">22 вересня, субота, 18:00<br/>Черкаська Філармонія</p>

    <?php
    if ( ! empty( $reg_button_link ) && ! empty( $reg_button_name ) ) : ?>
        <a class="registration-button"
           href="<?php echo $reg_button_link; ?>"><?php echo $reg_button_name; ?></a>
    <?php endif; ?>

    <?php
    if ( ! empty( $about_button_link ) && ! empty( $about_button_name ) ) : ?>
        <a class="about-button"
           href="<?php echo $about_button_link; ?>"><?php echo $about_button_name; ?></a>
    <?php endif; ?>
</div>
