<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>
<div class="season-ended">
    <h2 class="site-title"><?php echo get_season_name(); ?> сезон завершено</h2>
    <p class="site-sub-title page">Реєстрація на <?php echo get_next_season_number_ordinal(); ?> сезон відкриється 15 вересня</p>
</div>
