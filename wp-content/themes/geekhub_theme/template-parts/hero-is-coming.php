<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>

<div class="winter-is-coming">
    <h2 class="site-title">Новий, <?php echo get_season_number_ordinal(); ?> сезон вже близько!</h2>
    <p class="site-sub-title page">Реєстрація на вступні іспити розпочнеться: <br><span></span></p>
</div>
<script>
    $(function () {
        $('.winter-is-coming .site-sub-title span').countdown('<?php echo get_season_year(); ?>/08/15').on('update.countdown', function(event) {
            $(this).html(event.strftime('за %H год. та %M хв.'));
        }).on('finish.countdown', function(event) {
            $(this).html('за мить!');
        });
    });
</script>