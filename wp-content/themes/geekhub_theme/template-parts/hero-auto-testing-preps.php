<?php
/**
 * Template part for displaying hero section
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Official_GeekHub_Theme
 */

?>
<div class="auto-testing">
    <h2 class="site-title">Вступні іспити</h2>
    <p class="site-sub-title page">Розпочато збір ноутбуків.<br />Автотестування відбудеться<br />21 вересня</p>

    <a class="registration-button" target="_blank" href="https://www.facebook.com/geekhub.ck/posts/a.500291640055917/1909557305796003/?type=3&theater">Поділитися ноутбуком</a>

    <a class="about-button" target="_blank" href="/season-8/#114">Деталі процесу тестування</a>
</div>
