<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Official_GeekHub_Theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="site-info">
			<p class="site-footer-copyright"><?php echo get_theme_mod('address', 'м. Черкаси, вул. Горького, 60'); ?><br />Copyright &copy; GeekHub <?php echo date(' Y'); ?></p>

			<?php wp_nav_menu(array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu', 'menu_class' => 'site-footer-menu' )); ?>

			<ul class="site-social-wrapper">
				<li>
					<a target="_blank" href="<?php echo get_theme_mod('facebook', '#'); ?>">
						<i class="fa fa-facebook"></i>
					</a>
				</li>
				<li>
					<a target="_blank" href="<?php echo get_theme_mod('youtube', '#'); ?>">
						<i class="fa fa-youtube"></i>
					</a>
				</li>
			</ul>
		</div><!-- .site-info -->
	</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
