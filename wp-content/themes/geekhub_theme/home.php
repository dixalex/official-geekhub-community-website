<?php
/**
 * Template Name: Landing
 */
get_header(); ?>


<?php get_header() ?>

<!------------------------------ Section about -->

<section class="about-geekhub">
    <div class="container">
        <h3 class="section-title">
            <?php
            $about_title = get_theme_mod('about_geekhub_title', '');
            if (!empty($about_title)) : ?>
                <?php echo $about_title;
            endif; ?>
        </h3>

        <?php
        $about_description = get_theme_mod('about_geekhub_description', '');
        if (!empty($about_description)) : ?>
            <p><?php echo $about_description; ?></p>
        <?php endif; ?>
    </div>
</section>


<!---------------------------------- Section statistics -->
<section class="statistic-geekhub">
    <div class="background-overlay">

        <?php
        $statistic_image = get_theme_mod('geekhub_in_numbers_background', '');
        if (!empty($statistic_image)) : ?>
            <style>
                .statistic-geekhub {
                    background-image: url("<?php echo $statistic_image ?>");
                }
            </style>
        <?php endif; ?>

        <div class="container">
            <h3 class="section-title">
                <?php
                $statistic_title = get_theme_mod('geekhub_in_numbers_title', '');
                if (!empty($statistic_title)) : ?>
                    <?php echo $statistic_title;
                endif; ?>
            </h3>

            <ul class="statistic-features">

                <li class="wow bounceInLeft">
                    <h4 class="total"><?php
                        $statistic_features_seasons = get_theme_mod('seasons_number', '');
                        if (!empty($statistic_features_seasons)) : ?>
                            <?php echo $statistic_features_seasons;
                        endif; ?>
                    </h4>
                    <span><?php
                        $statistic_seasons = get_theme_mod('seasons_text', '');
                        if (!empty($statistic_seasons)) : ?>
                            <?php echo $statistic_seasons;
                        endif; ?>
			</span>
                </li>


                <li class="wow bounceInRight">
                    <h4 class="total"><?php
                        $statistic_features_students = get_theme_mod('students_number', '');
                        if (!empty($statistic_features_students)) : ?>
                            <?php echo $statistic_features_students;
                        endif; ?></h4>
                    <span><?php
                        $statistic_students = get_theme_mod('students_text', '');
                        if (!empty($statistic_students)) : ?>
                            <?php echo $statistic_students;
                        endif; ?>
			</span>
                </li>


                <li class="wow bounceInLeft">
                    <h4 class="total"><?php
                        $statistic_features_volunteer = get_theme_mod('volunteer_number', '');
                        if (!empty($statistic_features_volunteer)) : ?>
                            <?php echo $statistic_features_volunteer;
                        endif; ?>
                    </h4>
                    <span>
				<?php
                $statistic_volunteer = get_theme_mod('volunteer_text', '');
                if (!empty($statistic_volunteer)) : ?>
                    <?php echo $statistic_volunteer;
                endif; ?>
			</span>
                </li>


                <li class="wow bounceInRight">
                    <h4 class="total"><?php
                        $statistic_features_certify = get_theme_mod('certify_number', '');
                        if (!empty($statistic_features_certify)) : ?>
                            <?php echo $statistic_features_certify;
                        endif; ?></h4>
                    <span>
				<?php
                $statistic_certify = get_theme_mod('certify_text', '');
                if (!empty($statistic_certify)) : ?>
                    <?php echo $statistic_certify;
                endif; ?>
			</span>
                </li>


                <li class="wow bounceInLeft">
                    <h4 class="total"><?php
                        $statistic_features_registration = get_theme_mod('registration_number', '');
                        if (!empty($statistic_features_registration)) : ?>
                            <?php echo $statistic_features_registration;
                        endif; ?></h4>
                    <span>
				<?php
                $statistic_registration = get_theme_mod('registration_text', '');
                if (!empty($statistic_registration)) : ?>
                    <?php echo $statistic_registration;
                endif; ?>
			</span>
                </li>


                <li class="wow bounceInRight">
                    <h4 class="total"><?php
                        $statistic_features_worker = get_theme_mod('worker_number', '');
                        if (!empty($statistic_features_worker)) : ?>
                            <?php echo $statistic_features_worker;
                        endif; ?></h4>
                    <span>
				<?php
                $statistic_worker = get_theme_mod('worker_text', '');
                if (!empty($statistic_worker)) : ?>
                    <?php echo $statistic_worker;
                endif; ?>
			</span>
                </li>

            </ul>
        </div>
    </div>
</section>


<!------------------------- Section courses -->

<section class="courses background-overlay">
    <div class="container">

        <h3 class="section-title">
            <?php
            $courses_title = get_theme_mod('courses_list_title', '');
            if (!empty($courses_title)) : ?>
                <?php echo $courses_title;
            endif; ?>
        </h3>


        <?php $courses = new WP_Query(array('post_type' => 'gh_course', 'posts_per_page' => -1)); ?>

        <ul class="geekhub-courses">
            <?php if ($courses->have_posts()) : ?>
                <?php while ($courses->have_posts()) :
                    $courses->the_post(); ?>

                    <?php $post_slug = get_post_field('post_name', get_post()); ?>
                    <li>
                        <a
                                href="<?php echo get_permalink(get_theme_mod('our_courses_url')); ?>#<?php echo $post_slug ?>">
                            <?php if (has_post_thumbnail()) :
                                the_post_thumbnail();
                            endif; ?>

                            <h4 class="course-title"> <?php the_title(); ?> </h4>

                            <p><?php echo wp_trim_words(get_the_content(), 24, '...'); ?></p>

                            <span class="see-more"><?php esc_html_e('Детальніше', 'geekhub_theme') ?> <i
                                        class="fa fa-long-arrow-right"></i></span></a>
                    </li>
                <?php endwhile; ?>

            <?php else :
                get_template_part('template-parts/content', 'none');
            endif; ?>
            <?php wp_reset_query(); ?>
        </ul>
    </div>
</section>


<!------------------------- Section pre-footer -->
<section class="pre-footer">
    <div class="container">
        <ul class="tabs">
            <li class="wow bounceInUp">
                <?php $tab_url_1 = get_permalink(get_theme_mod('see_more_url1', ''));
                $tab_img_1 = get_theme_mod('see_more_img1', '');
                if (!empty($tab_url_1) && !empty($tab_img_1)) : ?>

                    <a href="<?php echo $tab_url_1 ?>"><img src="<?php echo $tab_img_1 ?>" alt=""></a>

                <?php endif; ?>
            </li>
            <li class="wow bounceInUp">
                <?php $tab_url_2 = get_permalink(get_theme_mod('see_more_url2', ''));
                $tab_img_2 = get_theme_mod('see_more_img2', '');
                if (!empty($tab_url_2) && !empty($tab_img_2)) : ?>

                    <a href="<?php echo $tab_url_2 ?>"><img src="<?php echo $tab_img_2 ?>" alt=""></a>

                <?php endif; ?>
            </li>
            <li class="wow bounceInUp">
                <?php $tab_url_3 = get_permalink(get_theme_mod('see_more_url3', ''));
                $tab_img_3 = get_theme_mod('see_more_img3', '');
                if (!empty($tab_url_3) && !empty($tab_img_3)) : ?>

                    <a href="<?php echo $tab_url_3 ?>"><img src="<?php echo $tab_img_3 ?>" alt=""></a>

                <?php endif; ?>
            </li>
        </ul>
    </div>
</section>

<?php get_footer() ?>
