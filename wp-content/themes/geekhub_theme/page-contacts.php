<?php
  /**
   * Template name: Contacts
   */

  get_header();
?>

<section class="contact-container">
  <div class="contact-address">
    <h3><?php echo get_theme_mod('contact_title_1', ''); ?></h3>
    <p class="contact-email">
      <a href="mailto:<?php echo get_theme_mod('contact_email', ''); ?>">
        <?php echo get_theme_mod('contact_email', ''); ?>
      </a>
    </p>
    <p class="contact-description"><?php echo get_theme_mod('contact_description', ''); ?></p>
  </div>
  <div class="contact-social">
    <h3><?php echo get_theme_mod('contact_title_2', ''); ?></h3>
    <ul class="site-social-wrapper">
      <li>
        <a target="_blank" href="<?php echo get_theme_mod('facebook', '#'); ?>">
          <span class="fa fa-facebook"></span>
        </a>
      </li>
      <li>
        <a target="_blank" href="<?php echo get_theme_mod('youtube', '#'); ?>">
          <span class="fa fa-youtube"></span>
        </a>
      </li>
    </ul>
  </div>
</section>
<div class="contact-map-container">
    <?php echo do_shortcode("[wpgmza id='1']"); ?>
</div>

<?php get_footer(); ?>