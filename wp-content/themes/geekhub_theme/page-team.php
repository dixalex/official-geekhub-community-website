<?php
/*
 * Template Name: page-team
 */
get_header();

function get_post_name ($post) {
	return $post->post_title;
}

function team_by_role($role, $heading) {
	?>
	<div class="container">
		<h2><?php echo $heading; ?></h2>
		<div class="masonry">
			<?php
				$query = new WP_Query(
					array(
						'post_type' => 'team_member',
						'posts_per_page' => -1,
						'orderby' => 'rand',
						'order' => 'ASC',
						'meta_query' => array(
							array(
									'key' => 'role',
									'value' => '"' . $role . '"',
    							'compare' => 'LIKE'
							)
						)
					)
				);

				if ($query->have_posts()) {
					while($query->have_posts()) {
						$query->the_post();

						if (has_post_thumbnail()) {
							?>
								<div class="member">
									<?php the_post_thumbnail( 'team_member' ); ?>
									<div class="info">
										<h3><?php the_title(); ?></h3>
										<p><?php echo get_field( "details" ); ?></p>
										<?php if(is_array(get_field("course"))): ?>
											<p>
												<?php echo join(", ", array_map("get_post_name", get_field("course"))); ?>
											</p>
										<?php endif; ?>
										<p class="seasons">СЕЗОНИ</p>
										<?php $seasons = get_the_terms($post->ID, "geekhub_season"); ?>
										<?php if(!empty($seasons)): ?>
											<ul>
												<?php foreach($seasons as $season): ?>
													<li><span><?php echo $season->name; ?></span></li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</div>
								</div>
							<?php
						}
					}
				}
			?>
		</div>
	</div>
	<?php
}

?>

<section class="team">
	<?php team_by_role("manager", "Команда менеджменту"); ?>
	<?php team_by_role("admin", "Адмін команда"); ?>
	<?php team_by_role("teacher", "Викладачі"); ?>
</section>

<?php get_footer();
