<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Official_GeekHub_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta property="og:image" content="<?php echo get_template_directory_uri() ?>/img/logo.jpg" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wptime-plugin-preloader"></div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text"
	   href="#content"><?php esc_html_e( 'Skip to content', 'geekhub_theme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-overlay">
			<div class="container">

				<h1 class="site-logo"><a href="<?php echo get_site_url(); ?>">
						<img src="<?php echo get_theme_mod( 'logo', 'Geekhub' ); ?>" alt="GeekHub">
					</a></h1>

				<nav id="site-navigation" class="gh-main-navigation" role="navigation">

					<span class="fa fa-bars"></span>

					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>

				</nav><!-- #site-navigation -->


				<?php
				if ( is_front_page() && is_home() ) : ?>
                    <div class="hero">
                        <?php
                            $mode = get_theme_mod( 'hero_mode' );
                            switch ($mode) {
                                case "default":
                                    get_template_part('template-parts/hero-season-ended');
                                    break;
                                case "countdown":
                                    get_template_part('template-parts/hero-is-coming');
                                    break;
                                case "register":
                                    get_template_part('template-parts/hero-registration-opened');
                                    break;
                                case "opening":
                                    get_template_part('template-parts/hero-season-opening');
                                    break;
                                case "testing":
                                    get_template_part('template-parts/hero-auto-testing');
                                    break;
                                case "testing_preparations":
                                    get_template_part('template-parts/hero-auto-testing-preps');
                                    break;
                                case "interview":
                                    get_template_part('template-parts/hero-interviews');
                                    break;
                                case "started":
                                    get_template_part('template-parts/hero-season-started');
                                    break;
                                default:
                                    get_template_part('template-parts/hero-season-ended');
                            }
                        ?>
                    </div>
				<?php else : ?>
					<div class="site-page-hero clearfix">
						<h2 class="site-title-page"><?php the_title(); ?></h2>
						<div class="site-sub-title-page clearfix"><?php the_excerpt(); ?></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
