<?php
/*
 * Template Name: page-faq
 */
get_header(); ?>

<div class="faq">
	<div class="container">
		<aside>
			<div class="aside-container">
				<ul class="menu-list">
					<?php $queryPost = new WP_Query(array('post_type' => 'gh_faq', 'posts_per_page' => 100, 'orderby' => 'date', 'order' => 'ASC'));
				       if ($queryPost->have_posts()) { 
							while ($queryPost->have_posts()) :
				            $queryPost->the_post(); ?>
				            <li>
				            	<a href="#<?php echo get_the_ID(); ?>"><?php echo get_field( "second_title" ); ?></a><span></span>
							</li>
				    		<?php endwhile; 
					}; ?>
				</ul>
				<a href="<?php the_permalink(get_theme_mod('hero_register_url')); ?>" class="btn btn-register">зареєструватися</a>
				<a href="<?php the_permalink(get_theme_mod('our_courses_url')); ?>" class="btn btn-our-courses">наші курси</a>
			</div>
		</aside>
		<main>
			<?php $queryPost = new WP_Query(array('post_type' => 'gh_faq', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'));
		       if ($queryPost->have_posts()) { 
					while ($queryPost->have_posts()) :
		            $queryPost->the_post(); ?>
		            <section id="<?php echo get_the_ID(); ?>">
						<h2><?php the_title(); ?></h2>
						<?php if (has_post_thumbnail()) :
							the_post_thumbnail(full) ;
							else : ?>

						<?php endif; ?>
						<?php the_content(); ?>
						<p><?php echo get_field( "short_details" );; ?></p>
			    	</section>
		    	<?php endwhile; 
			}; ?>
		</main>
	
	</div>
</div>


<?php
get_footer();