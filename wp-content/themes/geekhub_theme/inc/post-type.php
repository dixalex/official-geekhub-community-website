<?php


/* ------------------------------- Post type "Team members" ------------------ */
add_action( 'init', 'geekhub_team_members' );

function geekhub_team_members() {
	register_post_type( 'team_member', array(
		'public'   => true,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => false,
        'query_var'           => false,
		'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
		'labels'   => array(
			'name'      => __( 'Team members' ),
			'geekhub_theme',
			'add_new'   => 'Add new member',
			'all_items' => 'All team members',
		),
	) );
}


/* ------------------------- Custom category for team members ---------------- */

add_action( 'init', 'team_members_taxonomy' );

function team_members_taxonomy() {

	$labels = array(

		'name'          => __( 'Season' ),
		'singular_name' => __( 'Season' ),
		'search_items'  => __( 'Search' ),
		'all_items'     => __( 'All seasons' ),
		'add_new_item'  => __( 'Add new' ),
		'menu_name'     => __( 'Seasons' ),
	);

	$args = array(
		'labels' => $labels,
		'show_ui' => true,
        'show_tagcloud' => false,
        'hierarchical' => true

	);
	register_taxonomy( 'geekhub_season', 'team_member', $args );
}


/* ------------------------------- Post type "Course" ------------------ */
add_action( 'init', 'geekhub_courses' );

function geekhub_courses() {
	register_post_type( 'gh_course', array(
		'public'   => true,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => false,
        'query_var'           => false,
		'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
		'labels'   => array(
			'name'      => __( 'Courses' ),
			'geekhub_theme',
			'add_new'   => 'Add new course',
			'all_items' => 'All courses',
		),
	) );
}


/* ------------------------------- Post type "About season" ------------------ */
add_action( 'init', 'faq_post' );

function faq_post() {
	register_post_type( 'gh_faq', array(
		'public'   => true,
		'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
		'labels'   => array(
			'name'      => __( 'FAQ' ),
			'geekhub_theme',
			'add_new'   => 'Add new post',
			'all_items' => 'All posts',
		),
	) );
}