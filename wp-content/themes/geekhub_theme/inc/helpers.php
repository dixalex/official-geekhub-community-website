<?php

/**
 * Current Season Helpers
 */

function get_season_name()
{
    return get_theme_mod('season_title');
}
add_shortcode('season_name', 'get_season_name');


function get_season_number()
{
    return intval(get_theme_mod('season_number'));
}
add_shortcode('season_number', 'get_season_number');


function get_season_number_ordinal()
{
    return get_season_number() . 'ий';
}
add_shortcode('season_number_ordinal', 'get_season_number_ordinal');

function get_season_year()
{
    return '20' . (get_season_number() + 10);
}
add_shortcode('get_season_year', 'get_next_season_number');

/**
 * Next Season Helpers
 */

function get_next_season_number()
{
    return intval(get_theme_mod('season_number')) + 1;
}
add_shortcode('next_season_number', 'get_next_season_number');


function get_next_season_number_ordinal()
{
    return get_next_season_number() . 'ий';
}
add_shortcode('next_season_number_ordinal', 'get_next_season_number_ordinal');

function get_next_season_year()
{
    return '20' . (get_next_season_number() + 10);
}
add_shortcode('get_next_season_year', 'get_next_season_number');
