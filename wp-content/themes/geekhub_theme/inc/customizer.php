<?php
/**
 * Official GeekHub Theme Theme Customizer.
 *
 * @package Official_GeekHub_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function geekhub_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}

add_action( 'customize_register', 'geekhub_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function geekhub_theme_customize_preview_js() {
	wp_enqueue_script( 'geekhub_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}

add_action( 'customize_preview_init', 'geekhub_theme_customize_preview_js' );


//hero section
function hero_section_customize_register( $wp_customize ) {
    $wp_customize->add_section( 'hero_section', array(
        'title'    => __( 'Налаштування сезону', '' ),
        'priority' => 30,
    ) );

    $wp_customize->add_setting( 'season_title', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'season_title_control', array(
        'label'    => __( 'Ім’я сезону', '' ),
        'section'  => 'hero_section',
        'settings' => 'season_title',
    ) ) );

    $wp_customize->add_setting( 'season_number', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'season_number_control', array(
        'label'    => __( 'Номер сезону', '' ),
        'section'  => 'hero_section',
        'settings' => 'season_number',
    ) ) );

    $wp_customize->add_setting( 'hero_mode', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
        array(
            'default'    => 'default', //Default setting/value to save
            'type'       => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
            'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
            //'transport'  => 'postMessage', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
        )
    );

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize, //Pass the $wp_customize object (required)
        'hero_mode_control', //Set a unique ID for the control
        array(
            'label'      => __( 'Поточний статус сезону', 'hero_mode' ), //Admin-visible name of the control
            'description' => __( 'Using this option you can change the season mode (!)' ),
            'section'    => 'hero_section', //ID of the section this control should render in (can be one of yours, or a WordPress default section)
            'settings'   => 'hero_mode', //Which setting to load and manipulate (serialized is okay)
            'type'    => 'select',
            'choices' => array(
                'default' => 'Наступний сезон почнеться',
                'countdown' => 'Зворотній відлік',
                'register' => 'Реєстрація відкрита',
                'opening' => 'Відкриття сезону',
                'testing' => 'Тестування',
                'testing_preparations' => 'Тестування + Ноутбуки',
                'interview' => 'Співбесіди',
                'started' => 'Початок занятть',
            )
        )
    ) );


    $wp_customize->add_setting( 'hero_register_url', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'hero_register_url_control', array(
        'label'    => __( 'Red button action', '' ),
        'description' => __( 'Not always available' ),
        'section'  => 'hero_section',
        'settings' => 'hero_register_url',
        'type'     => 'dropdown-pages',
    ) ) );

    $wp_customize->add_setting( 'button_registration_text', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'button_text_1_control', array(
        'label'    => __( 'Red button Title', '' ),
        'section'  => 'hero_section',
        'settings' => 'button_registration_text',
    ) ) );

    $wp_customize->add_setting( 'hero_about_season_url', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'hero_about_season_url_control', array(
        'label'    => __( 'White button action', '' ),
        'description' => __( 'Not always available' ),
        'section'  => 'hero_section',
        'settings' => 'hero_about_season_url',
        'type'     => 'dropdown-pages',
    ) ) );

    $wp_customize->add_setting( 'button_about_text', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'button_text_2_control', array(
        'label'    => __( 'White button Title', '' ),
        'section'  => 'hero_section',
        'settings' => 'button_about_text',
    ) ) );


    $wp_customize->add_setting( 'our_courses_url', array(
        'default'   => '',
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'our_courses_url_control', array(
        'label'    => __( 'Courses button action', '' ),
        'section'  => 'hero_section',
        'settings' => 'our_courses_url',
        'type'     => 'dropdown-pages',
    ) ) );


    $wp_customize->add_setting( 'home_background' );

    $wp_customize->add_control(
        new WP_Customize_Image_Control( $wp_customize, 'hero_background_control',
            array(
                'label'    => __( 'Choose your home section background image', '' ),
                'section'  => 'title_tagline',
                'settings' => 'home_background',
            )
        )
    );
}

add_action( 'customize_register', 'hero_section_customize_register' );

//usual header non homepage
function logo_image_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'logo_image', array(
		'title'    => __( 'Header images', 'geekhub_theme' ),
		'priority' => 30,
	) );

	$wp_customize->add_setting( 'logo' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'logo_control',
			array(
				'label'    => __( 'Choose your logo image', '' ),
				'section'  => 'logo_image',
				'settings' => 'logo',
			)
		)
	);


	$wp_customize->add_setting( 'header_background' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'header_background_control',
			array(
				'label'    => __( 'Choose your header background image', '' ),
				'section'  => 'logo_image',
				'settings' => 'header_background',
			)
		)
	);
}

add_action( 'customize_register', 'logo_image_customize_register' );


//About geekhub section
function about_geekhub_section_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'about_geekhub_section', array(
		'title'    => __( 'About geekhub section', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'about_geekhub_title', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_geekhub_title_control', array(
		'label'    => __( 'Title', 'geekhub_theme' ),
		'section'  => 'about_geekhub_section',
		'settings' => 'about_geekhub_title',
	) ) );


	$wp_customize->add_setting( 'about_geekhub_description', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_geekhub_description_control', array(
		'label'    => __( 'Title', 'geekhub_theme' ),
		'section'  => 'about_geekhub_section',
		'settings' => 'about_geekhub_description',
		'type'     => 'textarea',
	) ) );

}

add_action( 'customize_register', 'about_geekhub_section_customize_register' );

//Geekhub in numbers section
function geekhub_in_numbers_section_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'geekhub_in_numbers_section', array(
		'title'    => __( 'Geekhub in numbers section', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'geekhub_in_numbers_title', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'geekhub_in_numbers_title_control', array(
		'label'    => __( 'Title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'geekhub_in_numbers_title',
	) ) );


	$wp_customize->add_setting( 'geekhub_in_numbers_background' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'geekhub_in_numbers_background_control',
			array(
				'label'    => __( 'Choose your background image', 'Rex' ),
				'section'  => 'geekhub_in_numbers_section',
				'settings' => 'geekhub_in_numbers_background',
			)
		)
	);


	$wp_customize->add_setting( 'seasons_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number1_control', array(
		'label'    => __( 'Seasons amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'seasons_number',
	) ) );


	$wp_customize->add_setting( 'seasons_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text1_control', array(
		'label'    => __( 'Seasons title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'seasons_text',
	) ) );


	$wp_customize->add_setting( 'registration_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number2_control', array(
		'label'    => __( 'Registration amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'registration_number',
	) ) );


	$wp_customize->add_setting( 'registration_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text2_control', array(
		'label'    => __( 'Registration title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'registration_text',
	) ) );


	$wp_customize->add_setting( 'students_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number3_control', array(
		'label'    => __( 'Students amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'students_number',
	) ) );


	$wp_customize->add_setting( 'students_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text3_control', array(
		'label'    => __( 'Students title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'students_text',
	) ) );


	$wp_customize->add_setting( 'certify_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number4_control', array(
		'label'    => __( 'Сertify amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'certify_number',
	) ) );


	$wp_customize->add_setting( 'certify_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text4_control', array(
		'label'    => __( 'Сertify title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'certify_text',
	) ) );


	$wp_customize->add_setting( 'worker_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number5_control', array(
		'label'    => __( 'Worker amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'worker_number',
	) ) );


	$wp_customize->add_setting( 'worker_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text5_control', array(
		'label'    => __( 'Worker title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'worker_text',
	) ) );


	$wp_customize->add_setting( 'volunteer_number', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'number6_control', array(
		'label'    => __( 'Volunteer amount', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'volunteer_number',
	) ) );


	$wp_customize->add_setting( 'volunteer_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'text6_control', array(
		'label'    => __( 'Volunteer title', 'geekhub_theme' ),
		'section'  => 'geekhub_in_numbers_section',
		'settings' => 'volunteer_text',
	) ) );

}

add_action( 'customize_register', 'geekhub_in_numbers_section_customize_register' );


// Support geekhub section

function support_geekhub_section_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'support_geekhub_section', array(
		'title'    => __( 'Support geekhub section', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'support_geekhub_title', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'support_geekhub_title_control', array(
		'label'    => __( 'Title', 'geekhub_theme' ),
		'section'  => 'support_geekhub_section',
		'settings' => 'support_geekhub_title',
	) ) );


	$wp_customize->add_setting( 'support_geekhub_text', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'support_geekhub_text_control', array(
		'label'    => __( 'Text', 'geekhub_theme' ),
		'section'  => 'support_geekhub_section',
		'settings' => 'support_geekhub_text',
		'type'     => 'textarea',
	) ) );

}

add_action( 'customize_register', 'support_geekhub_section_customize_register' );


//Courses list
function courses_list_section_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'courses_list_section', array(
		'title'    => __( 'Courses list section', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'courses_list_title', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'courses_list_title_control', array(
		'label'    => __( 'Title', 'geekhub_theme' ),
		'section'  => 'courses_list_section',
		'settings' => 'courses_list_title',
	) ) );

}

add_action( 'customize_register', 'courses_list_section_customize_register' );


//See more section
function see_more_section_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'see_more_section', array(
		'title'    => __( 'See more section', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'see_more_img1' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'see_more_img1_control',
			array(
				'label'    => __( 'Choose your first image', 'geekhub_theme' ),
				'section'  => 'see_more_section',
				'settings' => 'see_more_img1',
			)
		)
	);


	$wp_customize->add_setting( 'see_more_url1', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'see_more_url1_control', array(
		'label'    => __( 'Choose your first url', 'geekhub_theme' ),
		'section'  => 'see_more_section',
		'settings' => 'see_more_url1',
		'type'     => 'dropdown-pages',
	) ) );


	$wp_customize->add_setting( 'see_more_img2' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'see_more_img2_control',
			array(
				'label'    => __( 'Choose your second image', 'geekhub_theme' ),
				'section'  => 'see_more_section',
				'settings' => 'see_more_img2',
			)
		)
	);


	$wp_customize->add_setting( 'see_more_url2', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'see_more_url2_control', array(
		'label'    => __( 'Choose your second url', 'geekhub_theme' ),
		'section'  => 'see_more_section',
		'settings' => 'see_more_url2',
		'type'     => 'dropdown-pages',
	) ) );


	$wp_customize->add_setting( 'see_more_img3' );

	$wp_customize->add_control(
		new WP_Customize_Image_Control( $wp_customize, 'see_more_img3_control',
			array(
				'label'    => __( 'Choose your third image', 'geekhub_theme' ),
				'section'  => 'see_more_section',
				'settings' => 'see_more_img3',
			)
		)
	);


	$wp_customize->add_setting( 'see_more_url3', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'see_more_url3_control', array(
		'label'    => __( 'Choose your third url', 'geekhub_theme' ),
		'section'  => 'see_more_section',
		'settings' => 'see_more_url3',
		'type'     => 'dropdown-pages',
	) ) );

}

add_action( 'customize_register', 'see_more_section_customize_register' );


//for footer
function footer_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'footer', array(
		'title'    => __( 'Footer', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'facebook', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook_control', array(
		'label'    => __( 'Facebook', 'geekhub_theme' ),
		'section'  => 'footer',
		'settings' => 'facebook',
	) ) );


	$wp_customize->add_setting( 'twitter', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter_control', array(
		'label'    => __( 'Twitter', 'geekhub_theme' ),
		'section'  => 'footer',
		'settings' => 'twitter',
	) ) );


	$wp_customize->add_setting( 'vk', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'vk_control', array(
		'label'    => __( 'VK', 'geekhub_theme' ),
		'section'  => 'footer',
		'settings' => 'vk',
	) ) );


	$wp_customize->add_setting( 'youtube', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube_control', array(
		'label'    => __( 'Youtube', 'geekhub_theme' ),
		'section'  => 'footer',
		'settings' => 'youtube',
	) ) );


	$wp_customize->add_setting( 'address', array(
		'default'   => '',
		'transport' => 'refresh',
	) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'address_control', array(
		'label'    => __( 'Address', 'geekhub_theme' ),
		'section'  => 'footer',
		'settings' => 'address',
	) ) );


}

add_action( 'customize_register', 'footer_customize_register' );

function background_header() {
	?>

	<style type="text/css">
		.site-header {
			background-image: url("<?php if ( is_front_page() && is_home() ) {
          echo get_theme_mod('home_background');
        } else {
           echo get_theme_mod('header_background');
        }; ?>");
		}
	</style>

	<?php
}

add_action( 'wp_head', 'background_header' );

// Contacts
function contacts_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'contacts', array(
		'title'    => __( 'Contacts', 'geekhub_theme' ),
		'priority' => 30,
	) );


	$wp_customize->add_setting( 'contact_title_1', array(
		'default'   => '',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_title_1_control', array(
		'label'    => __( 'Title 1', 'geekhub_theme' ),
		'section'  => 'contacts',
		'settings' => 'contact_title_1',
	) ) );

	$wp_customize->add_setting( 'contact_title_2', array(
		'default'   => '',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_title_2_control', array(
		'label'    => __( 'Title 2', 'geekhub_theme' ),
		'section'  => 'contacts',
		'settings' => 'contact_title_2',
	) ) );

	$wp_customize->add_setting( 'contact_email', array(
		'default'   => '',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_email_control', array(
		'label'    => __( 'Email', 'geekhub_theme' ),
		'section'  => 'contacts',
		'settings' => 'contact_email',
	) ) );

	$wp_customize->add_setting( 'contact_description', array(
		'default'   => '',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_description_control', array(
		'label'    => __( 'Description', 'geekhub_theme' ),
		'section'  => 'contacts',
		'settings' => 'contact_description',
	) ) );

	$wp_customize->add_setting( 'contact_maps', array(
		'default'   => '',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'contact_maps_control', array(
		'label'       => __( 'Maps', 'geekhub_theme' ),
		'description' => __( 'Add shortcode' ),
		'section'     => 'contacts',
		'settings'    => 'contact_maps',
	) ) );
}

add_action( 'customize_register', 'contacts_customize_register' );
