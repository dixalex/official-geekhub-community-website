<?php
/**
 * Template Name: page-courses
 */
get_header(); ?>
    <section class="container-course">
        <div class="container">
            <div class="critique-container">
                <div class="courses-block">
                    <h4><?php esc_html_e('напрямки', 'geekhub_theme') ?></h4>
                    <ul>
                        <?php $queryPost1 = new WP_Query(array('post_type' => 'gh_course', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'));
                        if ($queryPost1->have_posts()) {
                            while ($queryPost1->have_posts()) :
                                $queryPost1->the_post(); ?>
                                <li>
                                    <?php $post_slug = get_post_field('post_name', get_post()); ?>
                                    <h3><a href="#<?php echo $post_slug ?>"><?php the_title(); ?></a></h3>
                                </li>
                            <?php endwhile;
                        }; ?>
                    </ul>
                </div>
            </div>
            <div class="tab-right">
                <?php $queryPost2 = new WP_Query(array('post_type' => 'gh_course', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC'));
                if ($queryPost2->have_posts()) {
                    while ($queryPost2->have_posts()) :
                $queryPost2->the_post(); ?>
                <?php $post_slug = get_post_field('post_name', get_post()); ?>
                <div id="<?php echo $post_slug; ?>">
                    <div class="tab-courses">
                        <?php the_post_thumbnail('gh_course'); ?>
                        <h3><?php the_title(); ?></h3>
                        <?php the_content(); ?>
                        <a class="registration-button"
                           href="<?php echo get_permalink(get_theme_mod('hero_register_url')); ?>"><?php esc_html_e('ЗАРЕЄСТРУВАТИСЯ', 'geekhub_theme') ?></a>
                    </div>

                    <?php if (get_field("program_text")): ?>
                        <div class="course-details-section">
                            <h4 class="course-details-section-title"><?php esc_html_e('Програма', 'geekhub_theme') ?></h4>
                            <div class="course-details-section-inner">
                                <?php echo get_field("program_text"); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (get_field("exam_questions_text")): ?>
                        <div class="course-details-section">
                            <h4 class="course-details-section-title"><?php esc_html_e('Питання до іспиту', 'geekhub_theme') ?></h4>
                            <div class="course-details-section-inner">
                                <?php echo get_field("exam_questions_text"); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (get_field("format_text")): ?>
                        <div class="course-details-section">
                            <h4 class="course-details-section-title"><?php esc_html_e('Формат курсу', 'geekhub_theme') ?></h4>
                            <div class="course-details-section-inner">
                                <?php echo get_field("format_text"); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (get_field("english_level_text")): ?>
                        <div class="course-details-section">
                            <h4 class="course-details-section-title"><?php esc_html_e('Рівень англійської', 'geekhub_theme') ?></h4>
                            <div class="course-details-section-inner">
                                <?php echo get_field("english_level_text"); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (get_field("companies_text")): ?>
                    <div class="course-details-section">
                        <h4 class="course-details-section-title"><?php esc_html_e('Компанії, які наймають із цією навичкою', 'geekhub_theme') ?></h4>
                        <div class="course-details-section-inner">
                        <?php echo get_field("companies_text"); ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if (get_field("selection_text")): ?>
                        <div class="course-details-section">
                            <h4 class="course-details-section-title"><?php esc_html_e('Яким буде відбір', 'geekhub_theme') ?></h4>
                            <div class="course-details-section-inner">
                                <?php echo get_field("selection_text"); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="course-details-section">
                        <h4 class="course-details-section-title"><?php esc_html_e('Викладачі', 'geekhub_theme') ?></h4>
                            <ul class="team-members">
                                <?php $queryPost = new WP_Query(array('post_type' => 'team_member',
                                    'posts_per_page' => -1,
                                    'orderby' => 'date',
                                    'order' => 'ASC',
                                    'meta_query' => array(
                                        array(
                                            'key' => 'course', // name of custom field
                                            'value' => get_the_ID(),
                                            'compare' => 'LIKE'
                                        )
                                    )
                                ));
                    if ($queryPost->have_posts()) {
                        while ($queryPost->have_posts()) :
                                        $queryPost->the_post(); ?>
                                        <li>


                                            <div class="seasons-star">
                                                <h5 class="seasons">СЕЗОНИ</h5>
                                                <?php
                                                $terms = wp_get_post_terms($post->ID, "geekhub_season", array( 'order' => 'ASC', 'orderby' => 'post_date'));
                        $count = count($terms);
                        // print_r ($terms) ;
                        if (!empty($terms)) {
                            echo "<ul>";
                            foreach ($terms as $term) {
                                echo "<li><span>" . $term->name . "</span></li>";
                            }
                            echo "</ul>";
                        } ?>
                                            </div>

                                            <div class="person">
                                                <?php the_post_thumbnail('team_member'); ?>
                                                <h3><?php the_title(); ?></h3>
                                                <?php the_content(); ?>
                                            </div>


                                        </li>
                                    <?php endwhile;
                    }; ?>
                            </ul>

                    </div>

                   
                    <a class="registration-button"
                       href="<?php echo get_permalink(get_theme_mod('hero_register_url')); ?>"><?php esc_html_e('ЗАРЕЄСТРУВАТИСЯ', 'geekhub_theme') ?> </a>


                </div>

                        <?php endwhile;
                }; ?>

            </div>
    </section>
<?php
get_footer();
